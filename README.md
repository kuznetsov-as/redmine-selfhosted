# Разворачиваем Redmine.

Создаем под проект папку
``` bash
mkdir ./redmine && cd ./redmine 
``` 

Создаем файл **Dockerfile**
``` bash
vim Dockerfile
``` 

``` bash
FROM redmine:latest
RUN apt-get update
```

Создаем файл **docker-compose.yml**
``` bash
vim docker-compose.yml
``` 

``` bash
version: '3.3'
services:
   postgres:
     image: postgres:10
     volumes:
       - ./storage/postgresql-data:/var/lib/postgresql/data
     environment:
       POSTGRES_PASSWORD: "strong_pass"
       POSTGRES_DB: "redmine"
       PGDATA: "/var/lib/postgresql/data"
     restart: unless-stopped
   redmine:
     build:
       context: .
     ports:
       - 80:3000
     volumes:
       - ./storage/docker_redmine-plugins:/usr/src/redmine/plugins
       - ./storage/docker_redmine-themes:/usr/src/redmine/public/themes
       - ./storage/docker_redmine-data:/usr/src/redmine/files
       - ./storage/configuration.yml:/usr/src/redmine/config/configuration.yml
     environment:
       REDMINE_DB_POSTGRES: "postgres"
       REDMINE_DB_USERNAME: "postgres"
       REDMINE_DB_PASSWORD: "strong_pass"
       REDMINE_DB_DATABASE: "redmine"
       REDMINE_SECRET_KEY_BASE: "…"
     restart: unless-stopped

```

Создаем папку storage
``` bash
mkdir ./storage && cd ./storage 
``` 

Создаем в ней **configuration.yml**
В username указываем почту, для password см. пункт "Настройка почты"
``` bash
vim configuration.yml
``` 

``` bash
production:
  email_delivery:
    delivery_method: :smtp
    smtp_settings:
      enable_starttls_auto: true
      address: "smtp.gmail.com"
      port: 587
      domain: "smtp.gmail.com"
      authentication: :plain
      user_name: ""
      password: ""

```

## Настройка почты
Чтобы работала почта, нужно создать в ней специальный пароль для приложений. Для этого в первую очередь необходимо включить двухфакторную авторизацию (до мая 2022 можно было без этого, но сейчас исключительно так).

Чтобы это сделать переходим в настройки гугл аккаунта -> безопасность -> двухфакторная авторизация. Включить ее можно будет тремя способами (добавлением номера телефона, входом с другого устройства, электронным ключом) - выбираем подходящий.

После этого появляется нужная нам настройка по пути: настройки гугл аккаунта -> безопасность -> пароли приложений 

![изображение](img/app-pass.JPG)

В качестве приложения выбираем email, в качестве устройства выбираем "другое" и пишем там что-то вроде "Redmine" (на свой вкус можно обзывать, главное, чтоб понятно было). 

Тыкаем на "создать", появляется пароль в желтой форме, копируем его и позже подставляем вместо пароля в configuration.yml

## Стартуем
``` bash
docker-compose down && docker-compose build && docker-compose up -d
```

На localhost:80 появится веб-морда, дефолтная учетка - логин admin, пароль admin.

После запуска обязательно заходим в **Администрирование** и во всплывающем сообщении нажмимаем **Загрузить конфигурацию по умолчанию**, иначе могут не работать многие функции.

После входа можем изменить почту админки ("Моя учетная запись" в правом верхнем углу экрана).

Затем на вкладке администрирование -> настройки -> уведомления по email можем тыкнуть на "послать email для проверки" (правый нижний угол экрана)

## Может пригодится
Исходная инструкция: https://kurazhov.ru/install-redmine-on-docker-compose/